import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:furniture_shop/helper/color_helper.dart';
import 'package:furniture_shop/routes/AppRoutes.dart';
import 'package:go_router/go_router.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05, top: heightSize * 0.3, bottom: heightSize * 0.08),
        width: widthSize,
        decoration: const BoxDecoration(
          image: DecorationImage(image: AssetImage('assets/images/livingroom.jpg'), fit: BoxFit.cover)
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: EdgeInsets.only(left: widthSize * 0.03, top: 0, bottom: 0),
              decoration: BoxDecoration(
                color: ColorHelper.grey.withOpacity(0.4),
                border: Border(
                  left: BorderSide(
                    color: ColorHelper.primary,
                    width: 4
                  )
                )
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Find Your', style: TextStyle(
                    color: ColorHelper.primary,
                    fontSize: heightSize * 0.05,
                    fontWeight: FontWeight.w600
                  )),
                  Text('Perfect Home', style: TextStyle(
                      color: ColorHelper.primary,
                      fontSize: heightSize * 0.05,
                      fontWeight: FontWeight.w600
                  )),
                ],
              ),
            ),
            GestureDetector(
              onTap: () => context.go(AppRoutes.HOME),
              child: Container(
                padding: EdgeInsets.only(top: heightSize * 0.02, bottom: heightSize * 0.02),
                width: widthSize * 0.8,
                decoration: BoxDecoration(
                    color: ColorHelper.dark,
                    borderRadius: BorderRadius.circular(15)
                ),
                child: Text('Get Started', textAlign: TextAlign.center, style: TextStyle(
                    color: ColorHelper.primary,
                    fontWeight: FontWeight.w600
                )),
              ),
            )
          ],
        ),
      ),
    );
  }

}