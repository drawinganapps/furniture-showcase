import 'package:flutter/material.dart';

class ColorHelper {
  static Color grey = const Color.fromRGBO(110, 110, 110, 1);
  static Color primary = const Color.fromRGBO(253, 253, 253, 1);
  static Color secondary = const Color.fromRGBO(246, 246, 246, 1.0);
  static Color tertiary = const Color.fromRGBO(255, 255, 255, 1);
  static Color quaternary = const Color.fromRGBO(229, 229, 231, 1);
  static Color yellow = const Color.fromRGBO(253, 168, 24, 1);
  static Color blue = const Color.fromRGBO(28, 85, 138, 1);
  static Color maron = const Color.fromRGBO(138, 28, 74, 1);
  static Color dark = const Color.fromRGBO(31, 35, 54, 1);
}
