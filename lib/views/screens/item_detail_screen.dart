import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:furniture_shop/helper/color_helper.dart';
import 'package:furniture_shop/routes/AppRoutes.dart';
import 'package:furniture_shop/views/pages/detail_item_page.dart';
import 'package:go_router/go_router.dart';

class ItemDetailScreen extends StatelessWidget {
  const ItemDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: ColorHelper.primary,
      appBar: AppBar(
        backgroundColor: ColorHelper.primary,
        elevation: 0,
        leadingWidth: widthSize * 0.2,
        leading: Center(
            child: GestureDetector(
              onTap: () => context.go(AppRoutes.HOME),
              child: Container(
                padding: const EdgeInsets.all(7),
                decoration: BoxDecoration(
                    color: ColorHelper.quaternary.withOpacity(0.2), shape: BoxShape.circle),
                child: Icon(Icons.arrow_back, color: ColorHelper.dark),
              ),
            )),
      ),
      body: const DetailItemPage(),
    );
  }

}