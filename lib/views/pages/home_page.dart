import 'package:flutter/material.dart';
import 'package:furniture_shop/helper/color_helper.dart';
import 'package:furniture_shop/resources/dummy_data.dart';
import 'package:furniture_shop/routes/AppRoutes.dart';
import 'package:furniture_shop/views/widgets/filter_widget.dart';
import 'package:furniture_shop/views/widgets/item_card_widget.dart';
import 'package:furniture_shop/views/widgets/search_widget.dart';
import 'package:go_router/go_router.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return ListView(
      children: [
        Container(
          margin: EdgeInsets.only(top: heightSize * 0.02),
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Find Your',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 25,
                          color: ColorHelper.dark)),
                  Text('Dream Furniture',
                      style: TextStyle(
                          fontWeight: FontWeight.w900,
                          fontSize: 25,
                          color: ColorHelper.dark)),
                ],
              ),
              SizedBox(
                height: heightSize * 0.06,
                child: Image.asset('assets/images/man.png', fit: BoxFit.cover),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(
              top: heightSize * 0.03, bottom: heightSize * 0.03),
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: const SearchWidget(),
        ),
        Container(
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Categories',
                  style: TextStyle(
                    color: ColorHelper.dark,
                    fontSize: 18,
                    fontWeight: FontWeight.w900,
                  )),
              Row(
                children: [
                  Text('See All',
                      style: TextStyle(color: ColorHelper.grey, fontSize: 12)),
                  Icon(
                    Icons.arrow_forward,
                    color: ColorHelper.dark,
                    size: 15,
                  )
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(
              top: heightSize * 0.03, bottom: heightSize * 0.03),
          height: heightSize * 0.1,
          child: ListView(
            physics: const BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            children: List.generate(DummyData.categories.length, (index) {
              return GestureDetector(
                onTap: () {},
                child: FilterWidget(
                    isSelected: index == 0,
                    filter: DummyData.categories[index]),
              );
            }),
          ),
        ),
        Container(
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Trendy',
                  style: TextStyle(
                    color: ColorHelper.dark,
                    fontSize: 18,
                    fontWeight: FontWeight.w900,
                  )),
              Row(
                children: [
                  Text('See All',
                      style: TextStyle(color: ColorHelper.grey, fontSize: 12)),
                  Icon(
                    Icons.arrow_forward,
                    color: ColorHelper.dark,
                    size: 15,
                  )
                ],
              )
            ],
          ),
        ),
        Container(
            margin: EdgeInsets.only(
                top: heightSize * 0.03, bottom: heightSize * 0.03),
            height: heightSize * 0.4,
            child: ListView(
              scrollDirection: Axis.horizontal,
              physics: const BouncingScrollPhysics(),
              children: List.generate(DummyData.items.length, (index) {
                return Container(
                  width: widthSize * 0.6,
                  margin: EdgeInsets.only(left: widthSize * 0.05),
                  child: GestureDetector(
                    onTap: () => context.go(AppRoutes.DETAILS),
                    child: ItemCardWidget(
                      item: DummyData.items[index],
                    ),
                  ),
                );
              }),
            ))
      ],
    );
  }
}
