import 'package:flutter/material.dart';
import 'package:furniture_shop/helper/color_helper.dart';
import 'package:furniture_shop/models/item.dart';

class ItemCardWidget extends StatelessWidget {
  final Item item;

  const ItemCardWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.only(
          left: widthSize * 0.03,
          right: widthSize * 0.03,
          top: heightSize * 0.01,
          bottom: heightSize * 0.01),
      decoration: BoxDecoration(
          color: ColorHelper.primary,
          borderRadius: BorderRadius.circular(25),
          image:
              DecorationImage(image: AssetImage('assets/images/${item.icon}'))),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [Icon(Icons.favorite, color: ColorHelper.dark)],
          ),
          Container(
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: ColorHelper.quaternary.withOpacity(0.5),
                borderRadius: BorderRadius.circular(15)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  child: Text(item.name,
                      style: TextStyle(
                          color: ColorHelper.dark,
                          fontWeight: FontWeight.w600)),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('\$ ${item.price.toStringAsFixed(2)}',
                        style: TextStyle(
                            fontSize: 15,
                            color: ColorHelper.dark,
                            fontWeight: FontWeight.w900)),
                    Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(right: 5),
                          child: Icon(Icons.star, color: ColorHelper.yellow, size: 18,),
                        ),
                        Text(item.ratings.toStringAsFixed(1),
                            style: TextStyle(
                                fontSize: 13,
                                color: ColorHelper.dark,
                                fontWeight: FontWeight.w500)),
                      ],
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
