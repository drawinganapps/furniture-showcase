import 'package:flutter/material.dart';
import 'package:furniture_shop/models/item.dart';

import '../models/category.dart';

class DummyData {
  static List<Category> categories = [
    Category(id: 1, name: 'Chair', icon: Icons.chair),
    Category(id: 2, name: 'Table', icon: Icons.event_seat),
    Category(id: 3, name: 'Bed', icon: Icons.bed),
    Category(id: 5, name: 'Lamp', icon: Icons.light),
    Category(id: 4, name: 'Set', icon: Icons.kitchen),
  ];

  static List<Item> items = [
    Item(id: 1, name: 'Stylish Wooden Chair', icon: 'chair1.jpg', price: 130.00, ratings: 5.0),
    Item(id: 1, name: 'Stylish Wooden Chair', icon: 'chair2.jpg', price: 130.00, ratings: 5.0),
    Item(id: 1, name: 'Stylish Wooden Chair', icon: 'chair3.jpg', price: 130.00, ratings: 5.0),
  ];

  static String productDescription = 'Soft but distinct lines create an elegant profile. The armchair’s nicely curved back provides your lumbar region with good support. And the cover in brown-pink adds warmth and energy.';
}