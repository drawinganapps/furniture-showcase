import 'package:furniture_shop/routes/AppRoutes.dart';
import 'package:furniture_shop/views/screens/home_screen.dart';
import 'package:furniture_shop/views/screens/item_detail_screen.dart';
import 'package:furniture_shop/views/screens/welcome_screen.dart';
import 'package:go_router/go_router.dart';

class AppPage {
  static var routes = GoRouter(
    initialLocation: AppRoutes.WELCOME,
    routes: [
      GoRoute(
        path: AppRoutes.WELCOME,
        builder: (context, state) => const WelcomeScreen(),
      ),
      GoRoute(
        path: AppRoutes.HOME,
        builder: (context, state) => const HomeScreen(),
      ),
      GoRoute(
        path: AppRoutes.DETAILS,
        builder: (context, state) => const ItemDetailScreen(),
      ),
    ],
  );
}
