import 'package:flutter/material.dart';
import 'package:furniture_shop/helper/color_helper.dart';
import 'package:furniture_shop/views/pages/home_page.dart';
import 'package:furniture_shop/views/widgets/navigation_widget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorHelper.secondary,
      body: const HomePage(),
      bottomNavigationBar: const NavigationWidget(selectedMenu: 0),
    );
  }
}