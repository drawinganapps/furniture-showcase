import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:furniture_shop/helper/color_helper.dart';
import 'package:furniture_shop/models/item.dart';
import 'package:furniture_shop/resources/dummy_data.dart';

class DetailItemPage extends StatelessWidget {
  const DetailItemPage({super.key});

  @override
  Widget build(BuildContext context) {
    Item item = DummyData.items[0];
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
      child: Column(
        children: [
          SizedBox(
            height: heightSize * 0.5,
            child: Image.asset('assets/images/${item.icon}'),
          ),
          Container(
            padding: EdgeInsets.all(widthSize * 0.03),
            height: heightSize * 0.4,
            decoration: BoxDecoration(
                color: ColorHelper.secondary,
                borderRadius: BorderRadius.circular(20)),
            child: ListView(
              physics: const BouncingScrollPhysics(),
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(item.name,
                            style: TextStyle(
                                color: ColorHelper.dark,
                                fontWeight: FontWeight.w600,
                                fontSize: 16)),
                        Container(
                          padding: const EdgeInsets.all(3),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: ColorHelper.yellow.withOpacity(0.1)),
                          child: Row(
                            children: [
                              Container(
                                margin: const EdgeInsets.only(right: 5),
                                child: Icon(
                                  Icons.star,
                                  color: ColorHelper.yellow,
                                  size: 16,
                                ),
                              ),
                              Text(item.ratings.toStringAsFixed(1),
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: ColorHelper.dark,
                                      fontWeight: FontWeight.w500)),
                            ],
                          ),
                        )
                      ],
                    ),
                    Text('Arm chair',
                        style: TextStyle(color: ColorHelper.grey, fontSize: 12))
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(
                      bottom: heightSize * 0.02, top: heightSize * 0.02),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: widthSize * 0.3,
                        height: heightSize * 0.06,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text('Color',
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                )),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      border:
                                          Border.all(color: ColorHelper.dark),
                                      shape: BoxShape.circle,
                                      color: ColorHelper.yellow),
                                ),
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: ColorHelper.blue),
                                ),
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: ColorHelper.maron),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(3),
                        decoration: BoxDecoration(
                            color: ColorHelper.primary,
                            borderRadius: BorderRadius.circular(15)),
                        child: Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.all(3),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: ColorHelper.quaternary),
                              child: Icon(
                                Icons.remove,
                                color: ColorHelper.dark,
                                size: 12,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  top: heightSize * 0.01,
                                  bottom: widthSize * 0.01),
                              child: Text('1',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w900,
                                      fontSize: 12,
                                      color: ColorHelper.dark)),
                            ),
                            Container(
                              padding: const EdgeInsets.all(3),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: ColorHelper.quaternary),
                              child: Icon(
                                Icons.add,
                                color: ColorHelper.dark,
                                size: 12,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  height: heightSize * 0.1,
                  child: Text(DummyData.productDescription,
                      overflow: TextOverflow.fade,
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                          fontSize: 14,
                          color: ColorHelper.dark.withOpacity(0.8))),
                ),
                Container(
                  margin: EdgeInsets.only(top: heightSize * 0.02),
                  padding: EdgeInsets.only(
                      left: widthSize * 0.05,
                      right: widthSize * 0.05,
                      top: heightSize * 0.02,
                      bottom: heightSize * 0.02),
                  decoration: BoxDecoration(
                      color: ColorHelper.dark,
                      borderRadius: BorderRadius.circular(25)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text('\$ ${item.price.toStringAsFixed(0)}',
                          style: TextStyle(color: ColorHelper.primary, fontWeight: FontWeight.w600)),
                      Text('Add to Cart',
                          style: TextStyle(color: ColorHelper.primary)),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
