import 'package:flutter/material.dart';
import 'package:furniture_shop/helper/color_helper.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: TextField(
        decoration: InputDecoration(
            contentPadding:
            const EdgeInsets.only(left: 15),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(color: ColorHelper.quaternary, width: 2)),
            filled: true,
            fillColor: ColorHelper.primary,
            hintText: 'Search...',
            prefixIcon: Icon(Icons.search, color: ColorHelper.dark, size: 25),
            suffixIcon: Container(
              margin: const EdgeInsets.only(top: 5, bottom: 5, right: 5),
              decoration: BoxDecoration(
                color: ColorHelper.quaternary,
                borderRadius: BorderRadius.circular(10)
              ),
              child: Icon(Icons.tune_rounded, color: ColorHelper.dark, size: 25),
            ),
            hintStyle: TextStyle(
                fontWeight: FontWeight.w500, color: ColorHelper.grey)),
      ),
    );
  }
}
