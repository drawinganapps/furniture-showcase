import 'package:flutter/material.dart';
import 'package:furniture_shop/helper/color_helper.dart';

class NavigationWidget extends StatelessWidget {
  final int selectedMenu;

  const NavigationWidget({super.key, required this.selectedMenu});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;

    Widget selectedItem(IconData icon, String name) => Container(
          padding: const EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: ColorHelper.dark, borderRadius: BorderRadius.circular(25)),
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.only(right: 5),
                child: Icon(icon, size: 20, color: ColorHelper.primary),
              ),
              Text(name,
                  style: TextStyle(color: ColorHelper.primary, fontSize: 12))
            ],
          ),
        );

    Widget nonSelectedItem(IconData icon) => Container(
          decoration: const BoxDecoration(shape: BoxShape.circle),
          child: Icon(icon, size: 25, color: ColorHelper.dark),
        );

    return Container(
      height: heightSize * 0.09,
      child: Container(
        decoration: BoxDecoration(
            color: ColorHelper.primary,
            borderRadius: BorderRadius.circular(35)),
        padding:
            EdgeInsets.only(left: widthSize * 0.03, right: widthSize * 0.03),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            selectedItem(Icons.home_rounded, 'Home'),
            nonSelectedItem(Icons.favorite_outline_rounded),
            Container(
                decoration: BoxDecoration(
                    border: Border.all(color: ColorHelper.dark, width: 3),
                    shape: BoxShape.circle),
                padding: EdgeInsets.all(heightSize * 0.015),
                child: nonSelectedItem(Icons.center_focus_strong)),
            nonSelectedItem(Icons.notifications_outlined),
            nonSelectedItem(Icons.shopping_bag_outlined),
          ],
        ),
      ),
    );
  }
}
