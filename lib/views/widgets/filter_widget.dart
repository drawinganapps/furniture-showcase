import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:furniture_shop/helper/color_helper.dart';
import 'package:furniture_shop/models/category.dart';

class FilterWidget extends StatelessWidget {
  final bool isSelected;
  final Category filter;

  const FilterWidget({Key? key, required this.isSelected, required this.filter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Center(
      child: Container(
        margin: EdgeInsets.only(left: screenWidth * 0.05),
        child: Container(
          padding: EdgeInsets.only(
              left: screenWidth * 0.05,
              right: screenWidth * 0.05,
              top: screenHeight * 0.02,
              bottom: screenHeight * 0.02),
          decoration: BoxDecoration(
              color: isSelected ? ColorHelper.dark : ColorHelper.tertiary,
              borderRadius: BorderRadius.circular(25)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
             Container(
               padding: const EdgeInsets.all(5),
               decoration: BoxDecoration(
                 color: isSelected ? ColorHelper.grey.withOpacity(0.5) : ColorHelper.secondary,
                 shape: BoxShape.circle
               ),
               child: Icon(filter.icon, size: 20, color: isSelected ? ColorHelper.primary : ColorHelper.dark),
             ),
              Text(filter.name,
                  style: TextStyle(
                      fontSize: 12,
                      color: isSelected ? ColorHelper.primary : ColorHelper.dark)),
            ],
          ),
        ),
      ),
    );

  }
}
