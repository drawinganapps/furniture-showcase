class Item {
  int id;
  String name;
  String icon;
  double price;
  double ratings;

  Item(
      {required this.id,
      required this.name,
      required this.icon,
      required this.price,
      required this.ratings});
}
